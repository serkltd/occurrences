## Occurrences

### GetOddOccurrences()

This is a method which, when provided with a slice of integers, returns only the integers which occur an odd number of times.

## Testing

Run `go test` to execute various test cases.