package occurrences

import (
	"sort"
)

// Returns slice containing only integers appearing an odd number of times
func GetOddOccurrences(numbers []int) []int {

	occurrences := make(map[int]int)
	var oddOccurrences []int

	// Count occurrences of each int in slice
	for _, num := range numbers {
		occurrences[num] = occurrences[num] + 1
	}

	// Build slice containing only integers appearing an odd number of times
	for k, v := range occurrences {
		if v%2 != 0 {
			oddOccurrences = append(oddOccurrences, k)
		}
	}

	// Sort slice into ascending order
	sort.Ints(oddOccurrences)

	return oddOccurrences
}