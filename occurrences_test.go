package occurrences

import(
	"github.com/stretchr/testify/assert"
	"testing"
)

// Define a model for test cases
type testCase struct {
	values			[]int
	expected		[]int
	test			string
}

// Define test cases
var (
	testCases = []testCase {
		{
			values:			[]int{1, 1, 2, 2, 2, 3, 3, 4, 4, 4, 5, 5},
			expected:		[]int{2, 4},
			test:			"Interview example",
		},
		{
			values:			[]int{1, 1, 1, 2, 2, 2, 3, 3, 4, 4, 4, 5, 5},
			expected:		[]int{1, 2, 4},
			test:			"3 odd occurrences",
		},
		{
			values:			[]int{1, 1, 2, 2, 3, 3, 4, 4, 5, 5},
			expected:		nil,
			test:			"No odd occurrences",
		},
		{
			values:			[]int{},
			expected:		nil,
			test:			"No values provided",
		},
	}
)

func TestGetOddOccurrences(t *testing.T) {
	// Range over test cases
	for _, testCase := range testCases {
		result := GetOddOccurrences(testCase.values)
		if !assert.Equal(t, testCase.expected, result) {
			t.Fatalf(":( Test Fail - %s\nResult: %v\nExpected result: %v", testCase.test, result, testCase.expected)
		}
		t.Logf(":) Test Pass - %s", testCase.test)
	}
}